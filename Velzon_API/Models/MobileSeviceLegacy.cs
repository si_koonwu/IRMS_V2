﻿using Microsoft.Data.SqlClient;

namespace Velzon_API.Models
{
    public class SQLManager
    {
        public SQLManager(string connectionString)
        {
            this.connectionString = connectionString;
        }

        readonly string app_version = "RMW v2.8.2.6 15-NOV-2021 3:00pm";
        readonly string version = System.Reflection.Assembly.GetExecutingAssembly()?.GetName()?.Version?.ToString() ?? "";
        public bool DEBUG_MODE = false;

        public readonly object locking = new object();

        public string connectionString;
        public string lockerConString;
        public bool enableLocker = false;

        enum logType
        {
            sql, error, info, debug, inspec, conv
        }

        #region testing
        public string SetDebug(bool debug, string callerip, string compname)
        {
            DEBUG_MODE = debug;
            string msg = "Turned debug mode " + (DEBUG_MODE ? "on" : "off");


            return msg;
        }


        public string testConnection()
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    con.Open();
                    con.Close();
                }
                return $"Connection to '{connectionString}' is successful.";
            }
            catch (Exception ex)
            {

                return $"Failed to connect '{connectionString}'.{Environment.NewLine}Error : {ex.Message}";
            }
        }

        public string getInfo()
        {
            return string.Format("Webservice matching Android version : {0}" + Environment.NewLine + "Webservice version : {1}", app_version, version);
        }

        public string getDBinfo()
        {
            return string.Format("Webservice DB : {0}", connectionString);
        }


    }
}
#endregion