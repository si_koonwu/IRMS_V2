﻿using System;
using System.Collections.Generic;

namespace Velzon_API.Models
{
    public partial class MMobileDevice
    {
        public int Id { get; set; }
        public string? MacAddr { get; set; }
        public string? Name { get; set; }
        public int? ProcessId { get; set; }

        public virtual MProcess? Process { get; set; }
    }
}
