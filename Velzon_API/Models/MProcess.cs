﻿using System;
using System.Collections.Generic;

namespace Velzon_API.Models
{
    public partial class MProcess
    {
        public int Id { get; set; }
        public string? Link { get; set; }
        public string? Name { get; set; }
        public int ProcessId { get; set; }

    }
}
