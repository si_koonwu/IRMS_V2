﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Velzon_API.Models
{
    public partial class irms2Context : DbContext
    {
        public irms2Context()
        {
        }

        public irms2Context(DbContextOptions<irms2Context> options)
            : base(options)
        {
        }

        public virtual DbSet<MMobileDevice> MMobileDevices { get; set; } = null!;
        public virtual DbSet<MProcess> MProcesses { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-DB3AA7F;Database=irms2;User Id=sa;Password=sophic1234;Trusted_Connection=False;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MMobileDevice>(entity =>
            {
                entity.ToTable("mMobileDevice");

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.MacAddr)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Name_");

                entity.HasOne(d => d.Process)
                    .WithMany()
                    .HasForeignKey(d => d.ProcessId)
                    .HasConstraintName("FK_mMobileDevice_ProcessId");
            });

            modelBuilder.Entity<MProcess>(entity =>
            {
                entity.ToTable("mProcess");

                entity.Property(e => e.Link)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasColumnName("Name_");

                entity.Property(e => e.ProcessId)
                   .HasMaxLength(300)
                   .IsUnicode(false)
                   .HasColumnName("ProcessId");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
