﻿using Microsoft.AspNetCore.Mvc;
using Velzon_API.Models;

namespace Velzon_API.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class ConnectionController : ControllerBase
    {
        SQLManager SQLManager;
        private IConfiguration _configuration;
        private irms2Context context;

        public ConnectionController(IConfiguration configuration, irms2Context context)
        {
            _configuration = configuration;
            this.context = context;
        }


        [HttpGet]
        [Route("testConnection")]
        public IActionResult testing()
        {

            try
            {
                if (context.Database.CanConnect())
                {
                    return Ok("Connected.");
                }
                else
                {
                    return Ok("Unable to connect to the database.");
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        /// <summary>
        /// Register or update the device info in DB
        /// </summary>
        /// <param name="DeviceName">The new device name</param>
        /// <param name="devid">The device ID stored in DB, if not registered before, leave as null.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("IdentifyDeviceName")]
        public IActionResult IdentifyDevice([FromQuery] string DeviceName, [FromQuery] int? DeviceID = null)
        {
            MMobileDevice? D;
            MMobileDevice? Checking = context.MMobileDevices.Where(x => x.Name.Equals(DeviceName)).FirstOrDefault();
            if (DeviceID != null)
            {
                D = context.MMobileDevices.Where(x => x.Id == DeviceID).FirstOrDefault();
                if (D == null)
                {
                    if (Checking == null)
                    {
                        //record not found, direct add new
                        var NewDeviceName = new MMobileDevice();
                        NewDeviceName.Name = DeviceName;
                        context.MMobileDevices.Add(NewDeviceName);
                        context.SaveChanges();

                        return Ok(new { Status = "Success", Message = NewDeviceName.Name + " register successfully", Data = NewDeviceName.Id });

                    }
                    else
                    {
                        //name in use
                        return Ok(new { Status = "Registered", Message = "Device UUID is in used" });
                    }

                } else
                {
                    if (Checking != null && Checking.Id != D.Id)
                    {
                        //name in use
                        return Ok(new { Status = "Registered", Message = "Device UUID is in used" });
                    }
                    else
                    {
                        //update the name
                        D.Name = DeviceName;
                        context.MMobileDevices.Update(D);
                        context.SaveChanges();
                    }
                }
            }
            else
            {
                // new device
                if (Checking == null)
                {
                    //register
                    var NewDeviceName = new MMobileDevice();
                    NewDeviceName.Name = DeviceName;
                    context.MMobileDevices.Add(NewDeviceName);
                    context.SaveChanges();

                    return Ok(new { Status = "Success", Message = NewDeviceName.Name + " register successfully", Data = NewDeviceName.Id });
                }
                else
                {
                    //in used
                    return Ok(new { Status = "Registered", Message = "Device UUID is in used." });
                }
            }
                

            return Ok(new { Status = "Update", Message = "Update Successfully" });
        }



        [HttpPost]
        [Route("UpdateProcess")]
        public IActionResult updateProcess([FromQuery] int ProcessID, string DeviceName)
        {
            var ProcessID1 = context.MProcesses.Where(d => d.Id.Equals(ProcessID)).FirstOrDefault();

            if (ProcessID == null)
                throw new Exception("Process Invalid.");

            var D = context.MMobileDevices.Where(n => n.Name.Equals(DeviceName)).FirstOrDefault();

            if (D == null)
                throw new Exception("Device is not register");

            D.ProcessId = ProcessID1.Id;
            context.MMobileDevices.Update(D);
            context.SaveChanges();

            return Ok(new { Status = "Success", Message = ProcessID1.Link });
        }



        [HttpPost]
        [Route("UpdateDeviceUUID")]
        public IActionResult UpdateDeviceUUID([FromQuery] int deviceID, string DeviceName)
        {
            var D = context.MMobileDevices.Where(n => n.Id.Equals(deviceID)).FirstOrDefault();
            var E = context.MMobileDevices.Where(n => n.Id != deviceID && n.Name.Equals(DeviceName)).FirstOrDefault();

            if (D != null)
            {

                if (E != null)
                {
                    //Exist = error message
                    return Ok(new { Status = "Existed" });
                }
                else
                {

                    D.Name = DeviceName;
                    context.MMobileDevices.Update(D);
                    context.SaveChanges();

                    return Ok(new { Status = "Update Success" });
                }

            }

            return Ok(new { Status = "Registed", Message = "ID is not registered." });
        }





        [HttpGet]
        [Route("GetAllProcess")]
        public IActionResult getAllProcess()
        {

            List<MProcess> processes = context.MProcesses.ToList();

            return Ok(processes);
        }


        [HttpPost]
        [Route("InsertServerLink")]
        public IActionResult insertServerLink([FromBody] ServerLink dto)
        {

            var insertServerLink = new MProcess();
            insertServerLink.Name = dto.Name;
            insertServerLink.Link = dto.Link;
            context.MProcesses.Add(insertServerLink);
            context.SaveChanges();
            return Ok(insertServerLink);
        }
    }

    public class modelDto
    {
        public int ProcessID { get; set; }

        public string DeviceName { get; set; }

        //and so on
    }

    public class ServerLink
    {

        public string Link { get; set; }
        public string Name { get; set; }

    }


}
